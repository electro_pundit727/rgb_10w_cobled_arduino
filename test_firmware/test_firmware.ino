const int interruptPin = 3; // interrupt pin for the button
const int RedPin = 11;      // Red pin of the 10W RGB LED
const int GreenPin = 10;    // Green pin of the 10W RGB LED
const int BluePin = 9;      // Blue pin of the 10W RGB LED

volatile int buttonCount;

#define redPwmValue 128
#define greenPwmValue 250
#define bluePwmValue 250

void setup() {
  
  Serial.begin(115200);
  pinMode(RedPin, OUTPUT);
  pinMode(GreenPin, OUTPUT);
  pinMode(BluePin, OUTPUT);

  pinMode(interruptPin, INPUT_PULLUP);  
  attachInterrupt(digitalPinToInterrupt(interruptPin), ColorSwitching, CHANGE);
  buttonCount = 0;
  
}

void loop() {
  
  switch (buttonCount) {
    case 0: {
        analogWrite(RedPin, 0);
        analogWrite(GreenPin, 0);
        analogWrite(BluePin, 0);

        Serial.println("LED is turned off.");
        break;
      }
    case 1: {
        analogWrite(RedPin, redPwmValue);
        analogWrite(GreenPin, 0);
        analogWrite(BluePin, 0);

        Serial.println("RED is turned on.");
        break;
      }
    case 2: {
        analogWrite(RedPin, 0);
        analogWrite(GreenPin, greenPwmValue);
        analogWrite(BluePin, 0);

        Serial.println("GREEN is turned on.");
        break;
      }
    case 3: {
        analogWrite(RedPin, 0);
        analogWrite(GreenPin, 0);
        analogWrite(BluePin, bluePwmValue);

        Serial.println("BLUE is turned on.");
        break;
      }
    case 4: {
        analogWrite(RedPin, redPwmValue);
        analogWrite(GreenPin, greenPwmValue);
        analogWrite(BluePin, bluePwmValue);

        Serial.println("WHITE is turned on.");
        break;
      }
    case 5: {
        analogWrite(RedPin, redPwmValue);
        analogWrite(GreenPin, greenPwmValue);
        analogWrite(BluePin, 0);

        Serial.println("YELLOW is turned on.");
        break;
      }
    default:
      Serial.println("Invalid entry.");
  } 
 
}

void ColorSwitching() {

  if (digitalRead(interruptPin) == LOW)
    buttonCount++;
  if (buttonCount > 5) {
    buttonCount = 0;
  }
  Serial.print("buttonCount: ");
  Serial.println(buttonCount);
  Serial.println("******************************************");
  
}
