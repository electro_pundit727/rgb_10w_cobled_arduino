This project is to control RGB 10W COB(Chip On the Board) with Atmega328P.

The project consist of two parts.

 1. Configuration of the prototype and Firmware for the testing of the prototype
 
    All of the components are following.
	
	- Arduino UNO 
	- RGB 10W COB 
	  https://www.amazon.co.uk/gp/product/B01DBZK64K/ref=ox_sc_act_title_2?smid=A29VZUSG1WM39I&psc=1
	- TIP122 NPN transistor
	- Push Botton
	- Passive components
	
2. PCB design with Atmega328P.